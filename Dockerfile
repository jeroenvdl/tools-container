# This image must only contain commonly used tools

FROM alpine

RUN apk add --no-cache \
    bash \
    jq \
    curl \
    git \
    openssl \
    python3 && \
    pip3 install --upgrade pip && \
    pip3 install awscli==1.16.313

WORKDIR /usr/local/bin

RUN curl -sSLO https://storage.googleapis.com/kubernetes-release/release/v1.17.0/bin/linux/amd64/kubectl && \
    chmod u+x kubectl

RUN wget -q -O kustomize.tar.gz \
    https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2Fv3.5.3/kustomize_v3.5.3_linux_amd64.tar.gz && \
    tar -xf kustomize.tar.gz && \
    chmod u+x kustomize
    
RUN curl -sSo aws-iam-authenticator https://amazon-eks.s3-us-west-2.amazonaws.com/1.14.6/2019-08-22/bin/linux/amd64/aws-iam-authenticator && \
    chmod u+x aws-iam-authenticator

